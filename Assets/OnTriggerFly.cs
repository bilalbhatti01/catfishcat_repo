﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OnTriggerFly : MonoBehaviour
{
    public List<GameObject> birds;

    public List<Transform> flyinyPoints;

    public float duration;

    private bool haveFled;

    private Vector3[] paths;

    public bool isLeft;



    private void Start()
    {
        GetPos();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("cat") && haveFled == false)
        {
            haveFled = true;
            Fly();
        }
    }

    private void Fly()
    {
        StartCoroutine(FlyCoroutine());
    }

    private void GetPos()
    {
        List<Vector3> points = new List<Vector3>();

        foreach (Transform ins in flyinyPoints)
        {
            points.Add(ins.position);
        }

        paths = points.ToArray();
    }

    IEnumerator FlyCoroutine()
    {
        for (int i = 0; i < birds.Count; i++)
        {
            birds[i].GetComponent<SpriteRenderer>().flipX = isLeft ? true : false;

            birds[i].GetComponent<Animator>().SetFloat("Fly", 1);
            birds[i].transform.DOLocalPath(paths, duration, PathType.CubicBezier, PathMode.Sidescroller2D);
            
            yield return new WaitForSeconds(0.2f);
        }
    }
}
