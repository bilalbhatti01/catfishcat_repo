﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class CutsceneUIController : MonoBehaviour
{

    public static CutsceneUIController instance;

    [Header(" Cutscene Frames ")]
    public Sprite[] cutsceneFrames;

    [Header("UI Elements")]
    public Button nextBtn;
    public Button backBtn;

    public Image cutsceneImage;
    public GameObject cutsceneCanvas;

    public bool cutsceneOver = false;
    public bool isEndGame = false;

    [SerializeField]
    private FMODUnity.StudioEventEmitter musicEmitter;

    [Header("Tween Properties")]





    private int index = 0;
    private void Awake()
    {
        instance = this;
    }


    public void Start()
    {
        nextBtn.onClick.AddListener(OnNextBtnClicked);
        backBtn.onClick.AddListener(OnBackBtnClicked);
    }

    [ContextMenu("Open Cutscene")]
    public void OpenCutscene()
    {
        cutsceneCanvas.GetComponent<RectTransform>().DOAnchorPosX(0, 1).SetEase(Ease.InBack); 
    }

    [ContextMenu("Close CutScene")]
    public void CloseCutScene()
    {
        cutsceneCanvas.GetComponent<RectTransform>().DOAnchorPosX(-1000,1).SetEase(Ease.OutBack);

        
    }

    private void ChangeFrame()
    {
        cutsceneImage.sprite = cutsceneFrames[index];
    }

    private void OnNextBtnClicked()
    {
        if (index == cutsceneFrames.Length - 1)
        {
            cutsceneOver = true;
            CloseCutScene();

            StartCoroutine(DelayLevelChange());

            return;
        }
        

        index++;
        if(index>0)
        {
            backBtn.gameObject.SetActive(true);
        }

        if(index==5 && !isEndGame)
        {
            SFXController.instance.PlayDoorSlam();
        }
        
        SFXController.instance.PlayUIClick();

        ChangeFrame();

        
    }

    private void OnBackBtnClicked()
    {
        index--;
        SFXController.instance.PlayUIClick();


        if (index == 0)
        {
            backBtn.gameObject.SetActive(false);
        }
        ChangeFrame();
    }

    IEnumerator DelayLevelChange()
    {
        if(!isEndGame)
        {

        
            //playerState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT)
            if (musicEmitter!=null)
            {
                musicEmitter.EventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }

            yield return new WaitForSeconds(2);

            SceneManager.LoadScene(1);

        }
        else
        {
            if(EndSceneManager.instance !=null)
            {
                EndSceneManager.instance.ShowDisclaimer();
            }
        }
    }

}
