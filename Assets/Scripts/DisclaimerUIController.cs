﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class DisclaimerUIController : MonoBehaviour
{
    public GameObject disclaimerGO;

    public GameObject[] disclaimerPartsGO;
    public Ease easeType_Disclaimer;

    [Header(" Tween Properties ")]
    public float toMiddleXPos;
    public float toLeftXPos;
    public float toRightXPos;

    public Button nextbtn;
    public Button backBtn;

    public bool inDisclaimer;

    private int index = 0;

    private void Start()
    {
        nextbtn.onClick.AddListener(OnNextBtnClicked);

        backBtn.onClick.AddListener(OnBackBtnClicked);
    }

    public void OnNextBtnClicked()
    {

        SFXController.instance.PlayUIClick();

        if(index==-1)
        {

        }
        else
        {
            MoveToLeft(disclaimerPartsGO[index].transform);
        }

        index++;

        if(index==1)
        {
            backBtn.gameObject.SetActive(true);
        }

        if(index==disclaimerPartsGO.Length)
        {
            if(inDisclaimer)
            {
                StartCoroutine(DelayGoToMainScene());

            }
            else
            {
                StartCoroutine(DelayGoToDis());
            }
        }
        else
        {
            MoveToMiddle(disclaimerPartsGO[index].transform);
        }
        
    }

    public void OnBackBtnClicked()
    {
        SFXController.instance.PlayUIClick();

        if(index==0)
        {
            
        }
        else
        {
            MoveToRight(disclaimerPartsGO[index].transform);

            index--;

            if(index==0)
            {
                backBtn.gameObject.SetActive(false);
            }

            MoveToMiddle(disclaimerPartsGO[index].transform);
        }

        

       
    }



    public void MoveToMiddle(Transform target)
    {
        target.GetComponent<RectTransform>().DOAnchorPosX(toMiddleXPos, 0.75f).SetEase(easeType_Disclaimer);
    }

    public void MoveToLeft(Transform target)
    {
        target.GetComponent<RectTransform>().DOAnchorPosX(toLeftXPos, 0.75f).SetEase(easeType_Disclaimer);
    }

    public void MoveToRight(Transform target)
    {
        target.GetComponent<RectTransform>().DOAnchorPosX(toRightXPos, 0.75f).SetEase(easeType_Disclaimer);
    }

    IEnumerator DelayGoToMainScene()
    {
        

        yield return new WaitForSeconds(1.0f);

        SceneManager.LoadScene(0);
    }

    IEnumerator DelayGoToDis()
    {
        EndSceneManager.instance.screenFadeInstance.FadeOut();

        yield return new WaitForSeconds(1.0f);

        EndSceneManager.instance.ShowDisclaimer();

        EndSceneManager.instance.screenFadeInstance.FadeIn();
    }

    public void OpenWindow()
    {
        disclaimerGO.SetActive(true);
    }

    public void CloseWindow()
    {
        disclaimerGO.SetActive(false);
    }

    
}
