﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneManager : MonoBehaviour
{
    public DisclaimerUIController catFishMesgInstance;
    public DisclaimerUIController discalimerMesgInstance;
    public ScreenFadeManager screenFadeInstance;

    public GameObject happyCutSceneGO;
    public GameObject BadCutSceneGO;

    public bool showDisclaimer = false;

    public static EndSceneManager instance;

    private void Awake()
    {
        instance = this;

        
    }



    private void Start()
    {
        StartEndGame();
    }

    public void StartEndGame()
    {
        StartCoroutine(EndGame_Coroutine());
    }

    IEnumerator EndGame_Coroutine()
    {
        yield return new WaitForSeconds(1);

        bool win = PlayerPrefs.GetInt("Win") == 1 ? true : false;

        JingleManager.instance.PlayJingle(win);

        if(win)
        {
            happyCutSceneGO.SetActive(true);
        }
        else
        {
            BadCutSceneGO.SetActive(true);
        }

        yield return new WaitForSeconds(1);

        screenFadeInstance.FadeIn();

        yield return new WaitWhile(() => showDisclaimer !=true);

        screenFadeInstance.FadeOut();



        if (win)
        {
            happyCutSceneGO.SetActive(false);
        }
        else
        {
            BadCutSceneGO.SetActive(false);
        }


        yield return new WaitForSeconds(1);

        screenFadeInstance.FadeIn();

        discalimerMesgInstance.OpenWindow();

    }

    public void ShowDisclaimer()
    {
        showDisclaimer = true;
    }
    
    
    public void setWin(int win)
    {
        PlayerPrefs.SetInt("Win", win);
    }

    
}
