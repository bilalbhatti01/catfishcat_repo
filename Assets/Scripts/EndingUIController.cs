﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingUIController : MonoBehaviour
{
    public static EndingUIController instance;

    public GameObject BadEnding;
    public GameObject GoodEnding;

    public Button nextbtn;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        nextbtn.onClick.AddListener(OnNextButtonPressed);
    }

    public void ShowBadEnd()
    {
        nextbtn.gameObject.SetActive(true);
        BadEnding.SetActive(true);
    }

    public void ShowGoodEnd()
    {
        nextbtn.gameObject.SetActive(true);
        GoodEnding.SetActive(true);
    }

    public void OnNextButtonPressed()
    {
        SceneManager.LoadScene(6);
        Debug.Log(" press ");
    }
}
