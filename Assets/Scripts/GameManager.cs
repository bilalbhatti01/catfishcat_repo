﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Range(1,5)]
    public int Chapter;

    public bool cutscen;

    private bool postWait=false;
    private bool chatWait = false;

    private float ThreatValue = 0;



    

    private ScreenFadeManager screenFadeManagerInstance;


    private void Awake()
    {
        instance = this;

        screenFadeManagerInstance = GetComponent<ScreenFadeManager>();
    }

    private void Start()
    {
        StartGame();
    }



    #region Story Event 1

    public void TriggerInitialThoughBubble_1()
    {
        StartCoroutine(InitalThoughtBubble_1_Coroutine());
    }
     
    IEnumerator InitalThoughtBubble_1_Coroutine()
    {
        ThoughtBubbleUIController.instance.ChangeBubbleText(" There is So much trouble at home right now...");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        yield return new WaitForSeconds(0.5f);

        ThoughtBubbleUIController.instance.ChangeBubbleText("I hope it won't last long.. :( ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

    }




    public void PrePostEvent_1()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ThoughtBubbleUIController.instance.ChangeBubbleText(" I am so mad but the sunset looks amazing today, I should take a picture. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();
    }





    public void TriggerPostEvent_1()
    {
        StartCoroutine(PostEvent_1_Coroutine());
    }

    IEnumerator PostEvent_1_Coroutine()
    {
        yield return new WaitForSeconds(1); /* Wait */

        ThoughtBubbleUIController.instance.CloseThoughtBubble(); /* Close thought bubble of the cat */

        yield return new WaitForSeconds(1); /* Wait */

        PostPicUIController.instance.TestPost1();

        yield return new WaitForSeconds(1);

        postWait = true;
        ResponseBtnUIController.instance.OpenYesNoContainer(); /* Brings the Yes no button up */

        yield return new WaitWhile(() => postWait != false); /* Wait till player presses  Yes  */

        ResponseBtnUIController.instance.CloseYesNoContainer();

        PostPicUIController.instance.ClosePost();   /*  Close the Post UI stuff  */


        PointClickMovement.instance.SwitchMovementLockState(false);

        ////////////////////////////////////////////////////////////////////

        //yield return new WaitForSeconds(5);

        //PointClickMovement.instance.SwitchMovementLockState(true);

        //ChatManager.instance.TestChat1();

        //chatWait = true;

        //yield return new WaitWhile(() => chatWait != false);

        //yield return new WaitForSeconds(5f);

        //ThoughtBubbleUIController.instance.ChangeBubbleText(" Who was that Cat?");

        //ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        //yield return new WaitForSeconds(5f);

        //ThoughtBubbleUIController.instance.CloseThoughtBubble();


    }





    public void TriggerChatEvent_1()
    {
        StartCoroutine(ChatEvent_1_Coroutine());
    }

    IEnumerator ChatEvent_1_Coroutine()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ChatManager.instance.TestChat1();

        chatWait = true;

        yield return new WaitWhile(() => chatWait != false);

        //yield return new WaitForSeconds(5f);

        
    }





    public void TriggerPostChatEvent_1()
    {
        StartCoroutine(PostChat_1_Coroutine());
    }

    IEnumerator PostChat_1_Coroutine()
    {
        


        // Second Thought
        ThoughtBubbleUIController.instance.ChangeBubbleText(" Having someone who understands my situation is nice...");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        PointClickMovement.instance.SwitchMovementLockState(true);

        PointClickMovement.instance.TurnAutoMoveOn(true);

        yield return new WaitForSeconds(4f);

        screenFadeManagerInstance.FadeOut();

        yield return new WaitForSeconds(4f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();


        AmbienceManager.instance.AudioFadeOut();

        PointClickMovement.instance.TurnAutoMoveOn(false);

        PointClickMovement.instance.SwitchMovementLockState(true);

        yield return new WaitForSeconds(3f);

        ChangeScene();

        //ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }


    public void LevelEnd_1()
    {

        screenFadeManagerInstance.FadeOut();

    }







    #endregion

    #region Story Event 2

    public void TriggerInitialThoughtBubble_2()
    {
        StartCoroutine(InitalThoughtBubble_2_Coroutine());
    }

    IEnumerator InitalThoughtBubble_2_Coroutine()
    {
        ThoughtBubbleUIController.instance.ChangeBubbleText(" I wonder who my secret messager is...");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        yield return new WaitForSeconds(0.5f);

        ThoughtBubbleUIController.instance.ChangeBubbleText("Something sweet would be nice right now... ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }


    public void PrePostEvent_2()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ThoughtBubbleUIController.instance.ChangeBubbleText(" Wow look at this Icecream !! ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();
    }



    public void TriggerPostEvent_2()
    {
        StartCoroutine(PostEvent_2_Coroutine());
    }

    IEnumerator PostEvent_2_Coroutine()
    {
        yield return new WaitForSeconds(1); /* Wait */

        ThoughtBubbleUIController.instance.CloseThoughtBubble(); /* Close thought bubble of the cat */

        yield return new WaitForSeconds(1); /* Wait */

        PostPicUIController.instance.TestPost2(); ;

        yield return new WaitForSeconds(1);

        postWait = true;
        ResponseBtnUIController.instance.OpenYesNoContainer(); /* Brings the Yes no button up */

        yield return new WaitWhile(() => postWait != false); /* Wait till player presses  Yes  */

        ResponseBtnUIController.instance.CloseYesNoContainer();

        PostPicUIController.instance.ClosePost();   /*  Close the Post UI stuff  */


        PointClickMovement.instance.SwitchMovementLockState(false);

       


    }



    public void TriggerChatEvent_2()
    {
        StartCoroutine(ChatEvent_2_Coroutine());
    }

    IEnumerator ChatEvent_2_Coroutine()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ChatManager.instance.TestChat2();

        chatWait = true;

        yield return new WaitWhile(() => chatWait != false);

        


    }


    public void TriggerPostChatEvent_2()
    {
        StartCoroutine(PostChat_2_Coroutine());
    }

    IEnumerator PostChat_2_Coroutine()
    {



        // Second Thought
        ThoughtBubbleUIController.instance.ChangeBubbleText(" This cat is really interested in me... But I need to hurry home, before mom gets mad.");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        PointClickMovement.instance.SwitchMovementLockState(true);

        PointClickMovement.instance.TurnAutoMoveOn(true);

        yield return new WaitForSeconds(8f);

        screenFadeManagerInstance.FadeOut();

        yield return new WaitForSeconds(4f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        AmbienceManager.instance.AudioFadeOut();

        PointClickMovement.instance.TurnAutoMoveOn(false);

        PointClickMovement.instance.SwitchMovementLockState(true);

        yield return new WaitForSeconds(3f);

        ChangeScene();

        //ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }


    #endregion

    #region Story Event 3


    public void TriggerInitialThoughBubble_3()
    {
        StartCoroutine(InitalThoughtBubble_3_Coroutine());
    }

    IEnumerator InitalThoughtBubble_3_Coroutine()
    {
        ThoughtBubbleUIController.instance.ChangeBubbleText(" All this trouble at home... I wanna get lost.");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        yield return new WaitForSeconds(0.5f);

        ThoughtBubbleUIController.instance.ChangeBubbleText("Have I been acting up yesterday... I hope they didnt get me wrong. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

    }




    public void PrePostEvent_3()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ThoughtBubbleUIController.instance.ChangeBubbleText(" Oh... this alley looks creepyyyy. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();
    }





    public void TriggerPostEvent_3()
    {
        StartCoroutine(PostEvent_3_Coroutine());
    }

    IEnumerator PostEvent_3_Coroutine()
    {
        yield return new WaitForSeconds(1); /* Wait */

        ThoughtBubbleUIController.instance.CloseThoughtBubble(); /* Close thought bubble of the cat */

        yield return new WaitForSeconds(1); /* Wait */

        PostPicUIController.instance.TestPost3();

        yield return new WaitForSeconds(1);

        postWait = true;
        ResponseBtnUIController.instance.OpenYesNoContainer(); /* Brings the Yes no button up */

        yield return new WaitWhile(() => postWait != false); /* Wait till player presses  Yes  */

        ResponseBtnUIController.instance.CloseYesNoContainer();

        PostPicUIController.instance.ClosePost();   /*  Close the Post UI stuff  */


        PointClickMovement.instance.SwitchMovementLockState(false);

       


    }





    public void TriggerChatEvent_3()
    {
        StartCoroutine(ChatEvent_3_Coroutine());
    }

    IEnumerator ChatEvent_3_Coroutine()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ChatManager.instance.TestChat3();

        chatWait = true;

        yield return new WaitWhile(() => chatWait != false);

        


    }





    public void TriggerPostChatEvent_3()
    {
        StartCoroutine(PostChat_3_Coroutine());
    }

    IEnumerator PostChat_3_Coroutine()
    {



        // Second Thought
        ThoughtBubbleUIController.instance.ChangeBubbleText(" Why are they so worried about my opinion of them...?");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        PointClickMovement.instance.SwitchMovementLockState(true);

        PointClickMovement.instance.TurnAutoMoveOn(true);

        yield return new WaitForSeconds(6f);

        screenFadeManagerInstance.FadeOut();

        yield return new WaitForSeconds(4f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        AmbienceManager.instance.AudioFadeOut();

        PointClickMovement.instance.TurnAutoMoveOn(false);

        PointClickMovement.instance.SwitchMovementLockState(true);

        yield return new WaitForSeconds(3f);

        ChangeScene();

        //ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }

    #endregion

    #region Story Event 4

    public void TriggerInitialThoughBubble_4()
    {
        StartCoroutine(InitalThoughtBubble_4_Coroutine());
    }

    IEnumerator InitalThoughtBubble_4_Coroutine()
    {
        ThoughtBubbleUIController.instance.ChangeBubbleText(" I wonder if they will message me again...");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        yield return new WaitForSeconds(0.5f);

        ThoughtBubbleUIController.instance.ChangeBubbleText("I am unsure how to feel about all these messages... ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

    }




    public void PrePostEvent_4()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ThoughtBubbleUIController.instance.ChangeBubbleText(" A bench, I could use a rest. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();
    }





    public void TriggerPostEvent_4()
    {
        StartCoroutine(PostEvent_4_Coroutine());
    }

    IEnumerator PostEvent_4_Coroutine()
    {
        yield return new WaitForSeconds(1); /* Wait */

        ThoughtBubbleUIController.instance.CloseThoughtBubble(); /* Close thought bubble of the cat */

        yield return new WaitForSeconds(1); /* Wait */

        PostPicUIController.instance.TestPost4();

        yield return new WaitForSeconds(1);

        postWait = true;
        ResponseBtnUIController.instance.OpenYesNoContainer(); /* Brings the Yes no button up */

        yield return new WaitWhile(() => postWait != false); /* Wait till player presses  Yes  */

        ResponseBtnUIController.instance.CloseYesNoContainer();

        PostPicUIController.instance.ClosePost();   /*  Close the Post UI stuff  */


        PointClickMovement.instance.SwitchMovementLockState(false);




    }





    public void TriggerChatEvent_4()
    {
        StartCoroutine(ChatEvent_4_Coroutine());
    }

    IEnumerator ChatEvent_4_Coroutine()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ChatManager.instance.TestChat4();

        chatWait = true;

        yield return new WaitWhile(() => chatWait != false);




    }





    public void TriggerPostChatEvent_4()
    {
        StartCoroutine(PostChat_4_Coroutine());
    }

    IEnumerator PostChat_4_Coroutine()
    {



        // Second Thought
        ThoughtBubbleUIController.instance.ChangeBubbleText(" Are they really just trying to be nice?");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        PointClickMovement.instance.SwitchMovementLockState(true);

        PointClickMovement.instance.TurnAutoMoveOn(true);

        yield return new WaitForSeconds(6f);

        screenFadeManagerInstance.FadeOut();

        yield return new WaitForSeconds(4f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        AmbienceManager.instance.AudioFadeOut();

        PointClickMovement.instance.TurnAutoMoveOn(false);

        PointClickMovement.instance.SwitchMovementLockState(true);

        yield return new WaitForSeconds(3f);

        ChangeScene();
        //ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }

    #endregion

    #region  Story Event 5

    public void TriggerInitialThoughBubble_5()
    {
        StartCoroutine(InitalThoughtBubble_5_Coroutine());
    }

    IEnumerator InitalThoughtBubble_5_Coroutine()
    {
        ThoughtBubbleUIController.instance.ChangeBubbleText(" I am really sick of this household.");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

        yield return new WaitForSeconds(0.5f);

        ThoughtBubbleUIController.instance.ChangeBubbleText("Maybe I should just run away. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        yield return new WaitForSeconds(6f);

        ThoughtBubbleUIController.instance.CloseThoughtBubble();

    }




    public void PrePostEvent_5()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ThoughtBubbleUIController.instance.ChangeBubbleText(" It is already getting dark... I should stay safe. ");

        ThoughtBubbleUIController.instance.PopOpenThoughtBubble();
    }





    public void TriggerPostEvent_5()
    {
        StartCoroutine(PostEvent_5_Coroutine());
    }

    IEnumerator PostEvent_5_Coroutine()
    {
        yield return new WaitForSeconds(1); /* Wait */

        ThoughtBubbleUIController.instance.CloseThoughtBubble(); /* Close thought bubble of the cat */

        yield return new WaitForSeconds(1); /* Wait */

        PostPicUIController.instance.TestPost5();

        yield return new WaitForSeconds(1);

        postWait = true;
        ResponseBtnUIController.instance.OpenYesNoContainer(); /* Brings the Yes no button up */

        yield return new WaitWhile(() => postWait != false); /* Wait till player presses  Yes  */

        ResponseBtnUIController.instance.CloseYesNoContainer();

        PostPicUIController.instance.ClosePost();   /*  Close the Post UI stuff  */


        PointClickMovement.instance.SwitchMovementLockState(false);




    }





    public void TriggerChatEvent_5()
    {
        StartCoroutine(ChatEvent_5_Coroutine());
    }

    IEnumerator ChatEvent_5_Coroutine()
    {
        PointClickMovement.instance.SwitchMovementLockState(true);

        ChatManager.instance.TestChat5();

        chatWait = true;

        yield return new WaitWhile(() => chatWait != false);




    }





    public void TriggerPostChatEvent_5()
    {
        StartCoroutine(PostChat_5_Coroutine());
    }

    IEnumerator PostChat_5_Coroutine()
    {



        // Second Thought
        //ThoughtBubbleUIController.instance.ChangeBubbleText(" Are they really just trying to be nice?");

        //ThoughtBubbleUIController.instance.PopOpenThoughtBubble();

        PointClickMovement.instance.SwitchMovementLockState(true);

        PointClickMovement.instance.TurnAutoMoveOn(true);

        yield return new WaitForSeconds(6f);

        screenFadeManagerInstance.FadeOut();

        yield return new WaitForSeconds(4f);

        AmbienceManager.instance.AudioFadeOut();

        PointClickMovement.instance.TurnAutoMoveOn(false);

        PointClickMovement.instance.SwitchMovementLockState(true);

        //yield return new WaitForSeconds(3f);
        //if(Win)
        //{
        //    EndingUIController.instance.ShowGoodEnd();

        //}
        //else
        //{
        //    EndingUIController.instance.ShowBadEnd();
        //}


        ChangeScene();
        //ThoughtBubbleUIController.instance.CloseThoughtBubble();
    }

    #endregion


    #region Story Related Functions

    public void ChangePostWaitState(bool state)
    {
        postWait = state;
    }

    public void ChangeChatWaitState(bool state)
    {
        chatWait = state;
    }

    #endregion


    #region Threat Related Funtions
    public void increaseThreat(float value)
    {
        if(ThreatValue  < 3)
        {
            ThreatValue += value;

        }
        if(ThreatValue>3)
        {
            ThreatValue = 3;
        }


    }

    public void DecreaseThreat(float value)
    {

        if (ThreatValue > -3)
        {
            ThreatValue -= value;

        }
        if (ThreatValue < -3)
        {
            ThreatValue = -3;
        }
    }
    #endregion


    #region Game Start
    private void StartGame()
    {
        StartCoroutine(SceneStartCoroutine());
    }

    IEnumerator SceneStartCoroutine()
    {
        AmbienceManager.instance.UpdateDangerParameter( PrefsManager.instance.GetDanger());

        PointClickMovement.instance.SwitchMovementLockState(true);

        if(cutscen)
        {
            CutsceneUIController.instance.OpenCutscene();

            yield return new WaitWhile(() => CutsceneUIController.instance.cutsceneOver != true);
        }

        screenFadeManagerInstance.TextFadeout();

        yield return new WaitForSeconds(3f);

        AmbienceManager.instance.PlayMusicAndAmbience();

        

        screenFadeManagerInstance.FadeIn();

        yield return new WaitForSeconds(2f);

        PointClickMovement.instance.SwitchMovementLockState(false);

    }

    #endregion

    #region Change Scene

    public void ChangeScene()
    {
        PrefsManager.instance.SaveDanger(AmbienceManager.instance.dangerParameter);

        SceneManager.LoadScene(Chapter + 1);
    }

    public void WinGame(int win)
    {
        PlayerPrefs.SetInt("Win", win);
    }
    #endregion


}
