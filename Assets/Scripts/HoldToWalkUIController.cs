﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HoldToWalkUIController : MonoBehaviour
{
    public Image holdToWalkimage;

    public float timeRemaining = 10;

    private bool imageShowing;

    


    public void Update()
    {
        TickTimer();
    }

    public void ShowHoldToWalk()
    {
        imageShowing = true;
        holdToWalkimage.DOFade(1, 1).SetEase(Ease.InOutBack);
    }

    public void CloseHoldToWalk()
    {
        imageShowing = false;
        holdToWalkimage.DOFade(0, 1).SetEase(Ease.InOutBack);
    }

    public void TickTimer()
    {
        if(!imageShowing)
        {
            if(!Input.GetMouseButton(0) && !PointClickMovement.instance.GetLockStatus())
            {
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;

                }
                else
                {
                    ShowHoldToWalk();
                }
            }
            else if(Input.GetMouseButtonDown(0))
            {
                timeRemaining = 10;
            }
            
        }
        else
        {
            if(Input.GetMouseButtonDown(0))
            {
                CloseHoldToWalk();
                timeRemaining = 10;
            }
        }
        
    }

    
}
