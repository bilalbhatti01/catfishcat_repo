﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class JingleManager : MonoBehaviour
{
    [SerializeField]
    [EventRef]
    private string happyJingleSoundEvent = null;

    [SerializeField]
    [EventRef]
    private string SadJingleSoundEvent = null;

    private FMOD.Studio.EventInstance eventInstance;

    public static JingleManager instance;

    private void Awake()
    {
        instance = this;
    }


    public void PlayJingle(bool win)
    {
        

        if(win)
        {
            eventInstance = RuntimeManager.CreateInstance(happyJingleSoundEvent);
        }
        else
        {
            eventInstance = RuntimeManager.CreateInstance(SadJingleSoundEvent);
        }

        eventInstance.start();
        
    }

    private void OnDestroy()
    {
        eventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
