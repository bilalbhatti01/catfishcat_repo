﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FMODUnity;
using TMPro;


public class MainMenuUIController : MonoBehaviour
{
    public GameObject playBtn;

    public GameObject creditsPanel;

    public Button settingsBtn;
    public Button creditsBtn;
    public Button creditExitBtn;
    public Button exitBtn;

    public Image fadeImage;

    public Image catBobImage;
    public TMP_Text headPhoneDisclaimer;

    public ScreenFadeManager screenFadeInstance;
   

    [SerializeField]
    [EventRef]
    private string mMenuMusicSoundEvent = null;

    private FMOD.Studio.EventInstance AudioEvent_MainMenu;

    [Header(" Tween Controls: ")]
    public Vector3 maxScaletween;
    public float tweenDuration;
    public Ease easeType;

    private void Start()
    {
        StartCoroutine(GameInit());

        playBtn.transform.DOScale(maxScaletween, tweenDuration).SetLoops(-1, LoopType.Yoyo).SetEase(easeType);

        MethodSubscribing();
    }

    private void MethodSubscribing()
    {
        playBtn.GetComponent<Button>().onClick.AddListener(PlayBtn_OnClickEvent);

        //settingsBtn.onClick.AddListener(SettingsBtn_OnClickEvent);

        creditsBtn.onClick.AddListener(CreditsBtn_OnClickEvent);

        creditExitBtn.onClick.AddListener(CreditsBtn_Exit);

        exitBtn.onClick.AddListener(ExitBtn_OnClickEvent);
    }

    private void PlayBtn_OnClickEvent()
    {
        StartCoroutine(GameStart_Coroutine());
        playBtn.GetComponent<Button>().interactable=false;
        settingsBtn.interactable = false;
        creditsBtn.interactable = false;
        exitBtn.interactable = false;
    }

    private void SettingsBtn_OnClickEvent()
    {
        Debug.Log("Settings menu");
    }

    private void ExitBtn_OnClickEvent()
    {
        Debug.Log("Exit btn");

        Application.Quit();


    }

    private void CreditsBtn_OnClickEvent()
    {
        SFXController.instance.PlayUIClick();

        Debug.Log(" Credtis Btn ");
        creditsPanel.GetComponent<RectTransform>().DOAnchorPosY(0, 0.75f).SetEase(Ease.OutBack);
    }

    private void CreditsBtn_Exit()
    {
        SFXController.instance.PlayUIClick();

        creditsPanel.GetComponent<RectTransform>().DOAnchorPosY(-1800, 0.75f).SetEase(Ease.InBack); ;
    }



    private void OnDestroy()
    {
        AudioEvent_MainMenu.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        AudioEvent_MainMenu.release();
    }

    public void FadeOut()
    {
        fadeImage.DOFade(1, 1);
    }

    IEnumerator GameStart_Coroutine()
    {
        ChangeMainMenuMusic();

        PrefsManager.instance.ResetDanger();

        FadeOut();

        yield return new WaitForSeconds(1.5f);

        DOTween.Kill(playBtn.transform);

        CutsceneUIController.instance.OpenCutscene();


    }

    IEnumerator GameInit()
    {
        yield return new WaitForSeconds(1.0f);

        ShowHeadPhoneDisclaimer();

        yield return new WaitForSeconds(5.0f);

        CloseHeadPhoneDisclaimer();

        yield return new WaitForSeconds(1.0f);

        InitializeFMODEvent_MainMenu();

        screenFadeInstance.FadeIn();
    }

    private void ChangeMainMenuMusic()
    {
        AudioEvent_MainMenu.setParameterByName("Cutscene",1);

    }

    public void InitializeFMODEvent_MainMenu()
    {
        AudioEvent_MainMenu = RuntimeManager.CreateInstance(mMenuMusicSoundEvent);
        AudioEvent_MainMenu.start();
    }

    private void ShowHeadPhoneDisclaimer()
    {
        headPhoneDisclaimer.DOFade(1, 1);
        catBobImage.DOFade(1, 1);
    }

    private void CloseHeadPhoneDisclaimer()
    {
        headPhoneDisclaimer.DOFade(0, 1);
        catBobImage.DOFade(0, 1);
    }

}
