﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MovesAtASpecificPosition : MonoBehaviour
{

    

    public float speed;
    public float wpRadius = 1;
    
    

    [Range(0.0f, 10.0f)] // create a slider in the editor and set limits on moveSpeed
    public float moveSpeed = 5f; // enemy move speed
    public float waitAtWaypointTime = 1f; // how long to wait at a waypoint before _moving to next waypoint

    public bool loop = true; // should it loop through the waypoints

    int _myWaypointIndex = 0;       // used as index for My_Waypoints
    float _moveTime;
    bool _moving = true;

    
    

    

   

    private GameObject wayPoint;
    private Animator _animator;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    private void Start()
    {

        AssignWaypoint();

        waitAtWaypointTime = Random.Range(15.0f, 20.0f);
        _moveTime = 0f;
        _moving = true;
        
        
    }
    private void Update()
    {
        // if beyond _moveTime, then start moving
        if (Time.time >= _moveTime)
        {
            Movement();
        }
    }

    void Movement()
    {
        // if there isn't anything in My_Waypoints
        if ((wayPoint != null) && (_moving))
        {

            // move towards waypoint
            transform.position = Vector3.MoveTowards(transform.position, wayPoint.transform.position, moveSpeed * Time.deltaTime);

            Vector3 dist = wayPoint.transform.position - transform.position;
            dist = dist.normalized;

            UpdateAnimatorParameters(dist.x);

            // if the enemy is close enough to waypoint, make it's new target the next waypoint
            if (Vector3.Distance(wayPoint.transform.position, transform.position) <= 0)
            {
                WayPointManager.instance.ReleaseWaypoint(_myWaypointIndex);

                AssignWaypoint();

                //_myWaypointIndex++;
                _moveTime = Time.time + waitAtWaypointTime;



            }

            // reset waypoint back to 0 for looping, otherwise flag not moving for not looping

        }
    }

    private void AssignWaypoint()
    {
        int temp = WayPointManager.instance.FindRandomUnTaken();

        if (temp != -1)
        {
            _myWaypointIndex = temp;
            wayPoint = WayPointManager.instance.wayPoints[_myWaypointIndex].wayPoint;
        }
    }

    private void UpdateAnimatorParameters(float val)
    {
        if(val<0)
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }



        _animator.SetFloat("Fly", Mathf.Abs(val));
    }
   

    

   

   


    


}
