﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class AmbienceManager : MonoBehaviour
{
    public static AmbienceManager instance;

    [SerializeField]
    [EventRef]
    private string bGMusicSoundEvent = null;

    [SerializeField]
    [EventRef]
    private string ambienceSoundEvent = null;


    private FMOD.Studio.EventInstance AudioEvent_Music;
    private FMOD.Studio.EventInstance AudioEvent_Amb;

    private FMOD.Studio.PARAMETER_ID dangerParameterId;

    [Range(-3,3)]
    public float dangerParameter;

    public void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        
        //AudioEvent.getParameterByName("Danger", out parameterEvent);


    }

    private void InitializeFMODEvent_BgMusic()
    {
        AudioEvent_Music = RuntimeManager.CreateInstance(bGMusicSoundEvent);

        FMOD.Studio.EventDescription dangerEventDescription;
        AudioEvent_Music.getDescription(out dangerEventDescription);

        FMOD.Studio.PARAMETER_DESCRIPTION dangerParameterDescription;
        dangerEventDescription.getParameterDescriptionByName("danger", out dangerParameterDescription);

        dangerParameterId = dangerParameterDescription.id;


        AudioEvent_Music.setParameterByName("DayCount",GameManager.instance.Chapter);


        AudioEvent_Music.start();
    }

    public void  InitializeFMODEvent_Amb()
    {
        AudioEvent_Amb = RuntimeManager.CreateInstance(ambienceSoundEvent);
        AudioEvent_Amb.start();
    }

    private void Update()
    {
        AudioEvent_Music.setParameterByID( dangerParameterId,dangerParameter);
    }

    public void UpdateDangerParameter(float value)
    {
        dangerParameter = value;
    }

    public void AddDecDanger(float val)
    {
        dangerParameter += val;

        if(dangerParameter > 3)
        {
            dangerParameter = 3;
        }
        else if(dangerParameter <-3)
        {
            dangerParameter = -3;
        }
    }

   



    public void PlayMusicAndAmbience()
    {
        InitializeFMODEvent_BgMusic();

        InitializeFMODEvent_Amb();
    }

    //public void ChangeDangerParameter(float val)
    //{
    //    studioEventEmit.SetParameter("Danger", val);   
    //    //parameterEvent = val;
    //}

    //[ContextMenu("More Danger")]
    //public void IncreaseDanger()
    //{
    //    ChangeDangerParameter(2);
    //}


    //[ContextMenu("Less Danger") ]
    //public void DecDanger()
    //{
    //    ChangeDangerParameter(-2);
    //}

    private void OnDestroy()
    {
        AudioEvent_Music.release();
        AudioEvent_Amb.release();
    }

    public void AudioFadeOut()
    {
        AudioEvent_Music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        AudioEvent_Amb.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
