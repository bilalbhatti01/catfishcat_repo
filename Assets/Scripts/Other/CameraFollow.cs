﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform catTransform;
    public Vector3 offset;

    private Vector3 temp;
    void Update()
    {
        temp = new Vector3(catTransform.position.x + offset.x, offset.y, offset.z); // Camera follows the player with specified offset position
       
    }

    private void LateUpdate()
    {
        transform.position = temp;
    }
}
