﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ChatInfo",menuName ="ChatMenu")]
public class Chat : ScriptableObject
{
    [TextArea(3, 15)]
    public string[] InitalFishDailouge;

    [TextArea(3, 15)]
    public string[] fishDialogues;

    
    public CatChatOptions[] catDialogues;


}

[System.Serializable]
public class CatChatOptions
{
    [TextArea(3, 15)]
    public string catDialogue;

    public float impactValue;
}
