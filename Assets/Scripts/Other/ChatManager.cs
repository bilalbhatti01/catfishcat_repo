﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatManager : MonoBehaviour
{
    [Header ("Chat Information")]
    public Chat chat1;
    public Chat chat2;
    public Chat chat3;
    public Chat chat4;
    public Chat chat5;

    [Header(" Post Sprite Information ")]
    

    private int currFishChat_State = 0;
    private int currCatChat_State = 0;

    private bool catReplyWait; /* Used for pausing the coroutine untill the player chooses the option on the keyboard */
    private bool catAnswerWait;

    public static ChatManager instance;

    private bool[] LocationOns = {false,false,false,false,false };


    


    private void Awake()
    {
        instance = this;
    }

    IEnumerator InitializeCoroutine_Chat1()
    {

       

        MessengerUIController.instance.DestroyMsgs();

        PointClickMovement.instance.SwitchMovementLockState(true);

        ResettingChatStates();




        catAnswerWait = true;

        ResponseBtnUIController.instance.OpenInstaIconContainer();

        yield return new WaitWhile(() => catAnswerWait != false);




        yield return new WaitForSeconds(1);


        // FIRST EXCHANGE STARTS ->
        if (LocationOns[0])
        {
            MessengerUIController.instance.CreateMessage(false, chat1.InitalFishDailouge[0], true); /* Fish First Message. */

        }
        else
        {
            MessengerUIController.instance.CreateMessage(false, chat1.InitalFishDailouge[1], true); /* Fish First Message. */
        }
        
        MessengerUIController.instance.OpenMessenger();




        /* Some Pause After 1st Message -> */  
        yield return new WaitForSeconds(2);


        #region CommentedOut
        //// Experiment
        //ThoughtBubbleUIController.instance.ChangeBubbleText(" A message? From a stranger? ");

        //ThoughtBubbleUIController.instance.PopOpenThoughtBubble();



        //yield return new WaitForSeconds(2f);





        //ResponseBtnUIController.instance.OpenAnswerContainer();



        //ResponseBtnUIController.instance.CloseAnswerContainer();







        //ThoughtBubbleUIController.instance.CloseThoughtBubble();

        //yield return new WaitForSeconds(0.5f);

        // Experiment 

        #endregion

        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat1.catDialogues[0].catDialogue, chat1.catDialogues[1].catDialogue }, new int[] { 0, 1 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();

        MessengerUIController.instance.CreateMessage(true, chat1.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat1.catDialogues[currCatChat_State].impactValue);

        /*---- PLAYER REPLY SECTION----- */



        // -> FIRST EXCHANGE ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State  ; /* Updating ChatState */



        // SECOND EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]); /* Fish First Message. */


            /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat1.catDialogues[2].catDialogue}, new int[] { 2 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat1.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat1.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> SECOND EXCHANGE  ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State ; /* Updating ChatState */


        // THIRD EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]); /* Fish First Message. */


            /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat1.catDialogues[3].catDialogue, chat1.catDialogues[4].catDialogue }, new int[] { 3 , 4 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat1.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat1.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> THIRD EXCHANGE ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State ; /* Updating ChatState */

        if (currFishChat_State == 3)
        {
            // FOURTH EXCHANGE -FIRST BRANCH STARTS ->
            MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]); /* Fish First Message. */


        }
        else
        {
            // FOURTH EXCHANGE  -SECOND BRANCH STARTS ->
            MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]); /* Fish First Message. */


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat1.catDialogues[5].catDialogue }, new int[] { 5 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();
            
            MessengerUIController.instance.CreateMessage(true, chat1.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat1.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */



            // -> FOURTH EXCHANGE ENDS


            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = currCatChat_State; /* Updating ChatState */


           

            MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]);
        }


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat1.catDialogues[6].catDialogue }, new int[] { 6 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        MessengerUIController.instance.CreateMessage(true, chat1.catDialogues[currCatChat_State].catDialogue);

        AmbienceManager.instance.AddDecDanger(chat1.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */

        // -> Fifth_Sisxt EXCHANGE ENDS



        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State; /* Updating ChatState */


        // FINAL MESSAGE

        MessengerUIController.instance.CreateMessage(false, chat1.fishDialogues[currFishChat_State]);



        yield return new WaitForSeconds(5);

            // CONVERSATION END

        MessengerUIController.instance.CloseMessenger();

        PointClickMovement.instance.SwitchMovementLockState(false);

        Debug.Log("Chat ENDED");



        


        yield return null;
    }

    IEnumerator InitializeCoroutine_Chat2()
    {
        MessengerUIController.instance.DestroyMsgs();

        PointClickMovement.instance.SwitchMovementLockState(true);

        ResettingChatStates();



        catAnswerWait = true;

        ResponseBtnUIController.instance.OpenInstaIconContainer();

        yield return new WaitWhile(() => catAnswerWait != false);

        yield return new WaitForSeconds(1);

        // FIRST EXCHANGE STARTS ->
        // FIRST EXCHANGE STARTS ->
        if (LocationOns[1])
        {
            MessengerUIController.instance.CreateMessage(false, chat2.InitalFishDailouge[0], true); /* Fish First Message. */

        }
        else
        {
            MessengerUIController.instance.CreateMessage(false, chat2.InitalFishDailouge[1], true); /* Fish First Message. */
        }

        MessengerUIController.instance.OpenMessenger();

        /* Some Pause After 1st Message -> */
        yield return new WaitForSeconds(2);




        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat2.catDialogues[0].catDialogue, chat2.catDialogues[1].catDialogue }, new int[] { 0, 1 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat2.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat2.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> FIRST EXCHANGE ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State ; /* Updating ChatState */



        // SECOND EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish First Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat2.catDialogues[2].catDialogue }, new int[] { 2 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat2.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat2.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> SECOND EXCHANGE  ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State; /* Updating ChatState */


        // THIRD EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish First Message. */

        currFishChat_State ++; /* Updating ChatState */
        yield return new WaitForSeconds(2);

        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish Second  Message. */

        currFishChat_State ++; /* Updating ChatState */
        yield return new WaitForSeconds(2);

        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish Third Message. */

        currFishChat_State++; /* Updating ChatState */
        yield return new WaitForSeconds(2);

        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish Fourth Message. */

        currFishChat_State++; /* Updating ChatState */
        yield return new WaitForSeconds(2);

        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish Fifth Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat2.catDialogues[3].catDialogue, chat2.catDialogues[4].catDialogue }, new int[] { 3, 4 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat2.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat2.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> THIRD EXCHANGE ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State ++; /* Updating ChatState */

        MessengerUIController.instance.CreateMessage(false, chat2.fishDialogues[currFishChat_State]); /* Fish Third Message. */



        yield return new WaitForSeconds(5);

        // XXX CONVERSATION END XXX 

        MessengerUIController.instance.CloseMessenger();

        PointClickMovement.instance.SwitchMovementLockState(false);

        Debug.Log("Chat ENDED");

        yield return null;
    }

    IEnumerator InitializeCoroutine_Chat3()
    {
        MessengerUIController.instance.DestroyMsgs();

        PointClickMovement.instance.SwitchMovementLockState(true);

        ResettingChatStates();



        catAnswerWait = true;

        ResponseBtnUIController.instance.OpenInstaIconContainer();

        yield return new WaitWhile(() => catAnswerWait != false);

        yield return new WaitForSeconds(1);



        // FIRST EXCHANGE STARTS ->
        if (LocationOns[2])
        {
            MessengerUIController.instance.CreateMessage(false, chat3.InitalFishDailouge[0], true); /* Fish First Message. */

        }
        else
        {
            MessengerUIController.instance.CreateMessage(false, chat3.InitalFishDailouge[1], true); /* Fish First Message. */
        }


        MessengerUIController.instance.OpenMessenger();

        /* Some Pause After 1st Message -> */  yield return new WaitForSeconds(2);



            /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat3.catDialogues[0].catDialogue, chat3.catDialogues[1].catDialogue }, new int[] { 0, 1 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat3.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat3.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */

        // -> FIRST EXCHANGE ENDS



        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = 0; ; /* Updating ChatState */


        // SECOND EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]); /* Fish First Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat3.catDialogues[2] .catDialogue, chat3.catDialogues[3].catDialogue }, new int[] { 2 , 3 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat3.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat3.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> SECOND EXCHANGE  ENDS


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State ++ ; /* Updating ChatState */


        // THIRD EXCHANGE STARTS ->
        MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]); /* Fish First Message. */


        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State++; /* Updating ChatState */


       
        MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]); /* Fish First Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat3.catDialogues[4].catDialogue, chat3.catDialogues[5].catDialogue }, new int[] { 4, 5 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();
        
        MessengerUIController.instance.CreateMessage(true, chat3.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat3.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        // -> THIRD EXCHANGE ENDS

        if (currCatChat_State == 5)
        {
            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State=4; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]); /* Fish First Message. */


            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State ++; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]); /* Fish First Message. */


            /* XXX  CONVO END  XXX */

            yield return new WaitForSeconds(5);

            MessengerUIController.instance.CloseMessenger();

            PointClickMovement.instance.SwitchMovementLockState(false);

            Debug.Log("Chat ENDED");

            yield return null;
        }
        else if(currCatChat_State == 4)
        {
            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State=3; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]);


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat3.catDialogues[6].catDialogue }, new int[] { 6 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();
            
            MessengerUIController.instance.CreateMessage(true, chat3.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat3.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */





            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State=6; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]);


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat3.catDialogues[7].catDialogue, chat3.catDialogues[8].catDialogue }, new int[] { 7,8 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();
            
            MessengerUIController.instance.CreateMessage(true, chat3.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat3.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */




            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 7; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat3.fishDialogues[currFishChat_State]);

            /* XXX  CONVO END  XXX */

            yield return new WaitForSeconds(5);

            MessengerUIController.instance.CloseMessenger();

            PointClickMovement.instance.SwitchMovementLockState(false);

            Debug.Log("Chat ENDED");

            yield return null;

        }

      

        
    }

    IEnumerator InitializeCoroutine_Chat4()
    {
        MessengerUIController.instance.DestroyMsgs();

        PointClickMovement.instance.SwitchMovementLockState(true);

        ResettingChatStates();



        catAnswerWait = true;

        ResponseBtnUIController.instance.OpenInstaIconContainer();

        yield return new WaitWhile(() => catAnswerWait != false);

        yield return new WaitForSeconds(1);



        // FIRST EXCHANGE STARTS ->
        if (LocationOns[3])
        {
            MessengerUIController.instance.CreateMessage(false, chat4.InitalFishDailouge[0], true); /* Fish First Message. */

            MessengerUIController.instance.OpenMessenger();

            yield return new WaitForSeconds(2);

            MessengerUIController.instance.CreateMessage(false, chat4.InitalFishDailouge[1], true); /* Fish First Message. */

        }
        else
        {
            MessengerUIController.instance.CreateMessage(false, chat4.InitalFishDailouge[2], true); /* Fish First Message. */

            MessengerUIController.instance.OpenMessenger();
        }


        

        /* Some Pause After 1st Message -> */
        yield return new WaitForSeconds(2);

        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat4.catDialogues[0].catDialogue, chat4.catDialogues[1].catDialogue }, new int[] { 0, 1 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();


        MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */



        yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
        currFishChat_State = currCatChat_State ; /* Updating ChatState */

        if(currCatChat_State == 0)
        {
            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]); /* Fish First Message. */


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat4.catDialogues[2].catDialogue, chat4.catDialogues[3].catDialogue }, new int[] { 2, 3 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */

            if (currCatChat_State == 3)
            {
                yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
                currFishChat_State = 2; /* Updating ChatState */
            }
            else if(currCatChat_State == 2)
            {
                yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
                currFishChat_State = 3; /* Updating ChatState */


                MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);


                yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
                currFishChat_State = 5; /* Updating ChatState */
            }

        }
        else if(currCatChat_State == 1)
        {
            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]); /* Fish First Message. */


            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 2; /* Updating ChatState */

        }

        if(currFishChat_State == 2)
        {
            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);



            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat4.catDialogues[4].catDialogue}, new int[] { 4 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */

            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 4; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);


            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 5; /* Updating ChatState */
        }

        MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);

        Debug.Log(" here ");

        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat4.catDialogues[5].catDialogue, chat4.catDialogues[6].catDialogue }, new int[] { 5,6 });
        MessengerUIController.instance.OpenKeyboard();
        Debug.Log(" here ");
        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();


        MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */


        if (currCatChat_State == 5)
        {
            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 6; /* Updating ChatState */



            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat4.catDialogues[8].catDialogue }, new int[] { 8 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */



            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 7; /* Updating ChatState */


            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat4.catDialogues[9].catDialogue, chat4.catDialogues[10].catDialogue }, new int[] { 9,10 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */

            if (currCatChat_State == 10)
            {
                yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
                currFishChat_State = 9; /* Updating ChatState */
            }
            else if(currCatChat_State==9)
            {
                yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
                currFishChat_State = 8; /* Updating ChatState */
            }
        }
        else if(currCatChat_State == 6)
        {

            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat4.catDialogues[7].catDialogue, }, new int[] { 7});
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat4.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat4.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */

            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 9; /* Updating ChatState */
        }

        if(currFishChat_State == 9)
        {
            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);

            /* XXX  CONVO END  XXX */

            yield return new WaitForSeconds(5);

            MessengerUIController.instance.CloseMessenger();

            PointClickMovement.instance.SwitchMovementLockState(false);

            Debug.Log("Chat ENDED");

            yield return null;
        }
        else if(currFishChat_State == 8)
        {
            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);

            yield return new WaitForSeconds(2); /* Pause -> awaiting Fishs Reply */
            currFishChat_State = 10; /* Updating ChatState */

            MessengerUIController.instance.CreateMessage(false, chat4.fishDialogues[currFishChat_State]);


            /* XXX  CONVO END  XXX */

            yield return new WaitForSeconds(5);

            MessengerUIController.instance.CloseMessenger();

            PointClickMovement.instance.SwitchMovementLockState(false);

            Debug.Log("Chat ENDED");

            yield return null;
        }

    }

    IEnumerator InitializeCoroutine_Chat5()
    {
        MessengerUIController.instance.DestroyMsgs();

        PointClickMovement.instance.SwitchMovementLockState(true);

        ResettingChatStates();




        catAnswerWait = true;

        ResponseBtnUIController.instance.OpenInstaIconContainer();

        yield return new WaitWhile(() => catAnswerWait != false);

        yield return new WaitForSeconds(1);


        // FIRST EXCHANGE STARTS ->
        if (LocationOns[4])
        {
            Debug.Log("here");
            MessengerUIController.instance.CreateMessage(false, chat5.InitalFishDailouge[0], true); /* Fish First Message. */

            MessengerUIController.instance.OpenMessenger();

           

        }
        else
        {
            Debug.Log("here");
            MessengerUIController.instance.CreateMessage(false, chat5.InitalFishDailouge[1], true); /* Fish First Message. */

            MessengerUIController.instance.OpenMessenger();
        }



        /* Some Pause After 1st Message -> */
        yield return new WaitForSeconds(2);

        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat5.catDialogues[0].catDialogue, chat5.catDialogues[1].catDialogue }, new int[] { 0, 1 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();


        MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */


        yield return new WaitForSeconds(2);
        currFishChat_State = 0;



        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false); /* Fish First Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat5.catDialogues[2].catDialogue, chat5.catDialogues[3].catDialogue }, new int[] { 2, 3 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();


        MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */


        yield return new WaitForSeconds(2);
        currFishChat_State = 1;



        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false); /* Fish First Message. */


        /*---- PLAYER REPLY SECTION----- */
        catReplyWait = true; // waiting for cat to reply.

        MessengerUIController.instance.CreateMultipleButtons(
            new string[] { chat5.catDialogues[4].catDialogue, chat5.catDialogues[5].catDialogue }, new int[] { 4, 5 });
        MessengerUIController.instance.OpenKeyboard();

        yield return new WaitWhile(() => catReplyWait != false);

        MessengerUIController.instance.CloseKeyboard();


        MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

        AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
        /*---- PLAYER REPLY SECTION----- */


        if (currCatChat_State == 4)
        {
            yield return new WaitForSeconds(2);
            currFishChat_State = 2;
        }
        else if (currCatChat_State == 5)
        {
            yield return new WaitForSeconds(2);
            currFishChat_State = 6;
        }


        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);

        yield return new WaitForSeconds(3);
        currFishChat_State++;

        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);

        yield return new WaitForSeconds(3);
        currFishChat_State ++;

        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);

        yield return new WaitForSeconds(3);
        currFishChat_State ++;

        MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);

       

        if(currFishChat_State == 5)
        {

            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[6].catDialogue, chat5.catDialogues[7].catDialogue }, new int[] { 6, 7 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */



            yield return new WaitForSeconds(2);
            currFishChat_State = currCatChat_State + 4;


            MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);


        }
        else if(currFishChat_State == 9)
        {

            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[8].catDialogue, chat5.catDialogues[9].catDialogue ,chat5.catDialogues[10].catDialogue }, new int[] { 8, 9 ,10});
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */




            if (currCatChat_State==8)
            {
                yield return new WaitForSeconds(2);
                currFishChat_State = 12;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);


                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] {  chat5.catDialogues[12].catDialogue }, new int[] {12 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */



            }
            else if(currCatChat_State == 9)
            {
                yield return new WaitForSeconds(2);
                currFishChat_State = 13;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);



                yield return new WaitForSeconds(2);
                currFishChat_State = 14;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);



                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[13].catDialogue , chat5.catDialogues[15].catDialogue }, new int[] { 13,15 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */


                if (currCatChat_State == 13)
                {
                    /*---- PLAYER REPLY SECTION----- */
                    catReplyWait = true; // waiting for cat to reply.

                    MessengerUIController.instance.CreateMultipleButtons(
                        new string[] { chat5.catDialogues[14].catDialogue }, new int[] { 14 });
                    MessengerUIController.instance.OpenKeyboard();

                    yield return new WaitWhile(() => catReplyWait != false);

                    MessengerUIController.instance.CloseKeyboard();


                    MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                    AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                    /*---- PLAYER REPLY SECTION----- */
                }
                else if( currCatChat_State == 15)
                {
                    yield return new WaitForSeconds(2);
                    currFishChat_State = 23;

                    MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                    /*--Fish Message */
                }




            }
            else if(currCatChat_State == 10)
            {
                yield return new WaitForSeconds(2);
                currFishChat_State = 15;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);



                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[16].catDialogue }, new int[] { 16 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */


                yield return new WaitForSeconds(2);
                currFishChat_State = 23;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

            }

        }

        if (currCatChat_State==12 || currFishChat_State == 11 || currFishChat_State == 10)
        {
            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[11].catDialogue }, new int[] { 11 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */
        }



        if(currCatChat_State == 11 || currCatChat_State == 14)
        {
            /*--Fish Message */
            yield return new WaitForSeconds(2);
            currFishChat_State = 16;

            MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
            /*--Fish Message */


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[17].catDialogue }, new int[] { 17 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */


            /*--Fish Message */
            yield return new WaitForSeconds(2);
            currFishChat_State = 17;

            MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
            /*--Fish Message */



            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[18].catDialogue }, new int[] { 18 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */


            /*--Fish Message */
            yield return new WaitForSeconds(2);
            currFishChat_State = 18;

            MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
            /*--Fish Message */


            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[19].catDialogue, chat5.catDialogues[20].catDialogue }, new int[] { 19, 20 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */




            if (currCatChat_State == 19)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(2);
                currFishChat_State = 19;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 20;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[21].catDialogue }, new int[] { 21 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */





            }
            else if (currCatChat_State == 20)
            {

                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 22;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */


                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[21].catDialogue, chat5.catDialogues[22].catDialogue }, new int[] { 21, 22 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */



            }

            if (currCatChat_State == 21)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 21;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */
            }
            else if (currCatChat_State == 22)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 23;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */
            }
        }


        if(currFishChat_State == 23)
        {




            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[23].catDialogue, chat5.catDialogues[24].catDialogue }, new int[] { 23, 24 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */


            if (currCatChat_State == 23)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 24;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */


                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[25].catDialogue, chat5.catDialogues[26].catDialogue }, new int[] { 25, 26 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */



            }
            else if(currCatChat_State == 24)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 25;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

                /*--Fish Message */
                yield return new WaitForSeconds(2);
                currFishChat_State = 26;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */


                /*--Fish Message */
                yield return new WaitForSeconds(1.5f);
                currFishChat_State = 27;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */
            }

            if(currCatChat_State==25)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 29;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] { chat5.catDialogues[27].catDialogue, chat5.catDialogues[28].catDialogue }, new int[] { 27, 28 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */

                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 30;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */

            }

            if (currCatChat_State == 26 || currFishChat_State == 27)
            {
                /*--Fish Message */
                yield return new WaitForSeconds(3);
                currFishChat_State = 28;

                MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                /*--Fish Message */


            }

            if(currFishChat_State == 28 || currFishChat_State == 30)
            {
                /*---- PLAYER REPLY SECTION----- */
                catReplyWait = true; // waiting for cat to reply.

                MessengerUIController.instance.CreateMultipleButtons(
                    new string[] {  chat5.catDialogues[29].catDialogue, chat5.catDialogues[30].catDialogue }, new int[] { 29,30 });
                MessengerUIController.instance.OpenKeyboard();

                yield return new WaitWhile(() => catReplyWait != false);

                MessengerUIController.instance.CloseKeyboard();


                MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

                AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
                /*---- PLAYER REPLY SECTION----- */


                if (currCatChat_State== 29)
                {
                    /*--Fish Message */
                    yield return new WaitForSeconds(3);
                    currFishChat_State = 31;

                    MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                    /*--Fish Message */


                    //END

                    /* XXX  CONVO END  XXX */

                    yield return new WaitForSeconds(5);

                    MessengerUIController.instance.CloseMessenger();

                    PointClickMovement.instance.SwitchMovementLockState(false);

                    Debug.Log("Chat ENDED");

                    GameManager.instance.WinGame(0);

                    yield return null;


                }
                else if(currCatChat_State==30)
                {
                    /*--Fish Message */
                    yield return new WaitForSeconds(3);
                    currFishChat_State = 32;

                    MessengerUIController.instance.CreateMessage(false, chat5.fishDialogues[currFishChat_State], false);
                    /*--Fish Message */


                    
                }

            }

            





        }
        if (currFishChat_State == 32 || currFishChat_State == 21)
        {
            /*---- PLAYER REPLY SECTION----- */
            catReplyWait = true; // waiting for cat to reply.

            MessengerUIController.instance.CreateMultipleButtons(
                new string[] { chat5.catDialogues[31].catDialogue }, new int[] { 31 });
            MessengerUIController.instance.OpenKeyboard();

            yield return new WaitWhile(() => catReplyWait != false);

            MessengerUIController.instance.CloseKeyboard();


            MessengerUIController.instance.CreateMessage(true, chat5.catDialogues[currCatChat_State].catDialogue); /* Cat Replys */

            AmbienceManager.instance.AddDecDanger(chat5.catDialogues[currCatChat_State].impactValue);
            /*---- PLAYER REPLY SECTION----- */

            //END

            /* XXX  CONVO END  XXX */

            yield return new WaitForSeconds(5);

            MessengerUIController.instance.CloseMessenger();

            PointClickMovement.instance.SwitchMovementLockState(false);

            Debug.Log("Chat ENDED");

            GameManager.instance.WinGame(1);

            yield return null;
        }





























    }



    private void ResettingChatStates()
    {
        currCatChat_State = 0;
        currFishChat_State = 0;

        Debug.Log("Chat States Reseted");
    }

    public void ChangeCatChatState(int state)
    {
        
        currCatChat_State = state;
        
        catReplyWait = false;
    }

    public void ChangeCatAnswerState()
    {
        catAnswerWait = false;
    }

    


    #region TestCases

    [ContextMenu(" Start Chat 1 Testing")]
    public void TestChat1()
    {

        StartCoroutine(InitializeCoroutine_Chat1());
    }

    [ContextMenu(" Start Chat 2 Testing ")]
    public void TestChat2()
    {
        StartCoroutine(InitializeCoroutine_Chat2());
    }


    [ContextMenu(" Start Chat 3 Testing ")]
    public void TestChat3()
    {
        StartCoroutine(InitializeCoroutine_Chat3());
    }

    [ContextMenu(" Start Chat 4 Testing ")]
    public void TestChat4()
    {
        StartCoroutine(InitializeCoroutine_Chat4());
    }


    [ContextMenu(" Start Chat 5 Testing ")]
    public void TestChat5()
    {
        StartCoroutine(InitializeCoroutine_Chat5());
    }

    #endregion


    public void SwitchLocationState(int index, bool value)
    {
        if(index <LocationOns.Length && index > -1)
        {
            LocationOns[index] = value;

        }

        Debug.Log($"Location {index} turned On {value}");
    }
}
