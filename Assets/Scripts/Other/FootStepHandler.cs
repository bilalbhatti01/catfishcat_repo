﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FootStepHandler : MonoBehaviour
{
    [EventRef]
    public string  footStep_SoundEvent;
    
    public float waitTime = 0.75f;
    
    
    public bool Steping = false;
    private bool isMoving = false;

    void Update()
    {
        if (isMoving && !Steping)
        {
            Invoke("PlayFootStepSFX", 0);
            Debug.Log("Steped");
        }

    }

    public void PlayFootStepSFX()
    {
        Steping = true;

        RuntimeManager.PlayOneShot(footStep_SoundEvent);

        Invoke("ChangeStep", waitTime);

    }

    void ChangeStep()
    {
        Steping = false;
    }

    public void SwitchIsMoving(bool state)
    {
        isMoving = state;
    }

    #region Commented out Old Code
    //public AudioClip[] FootStepSounds;
    //public float waitTime = 0.75f;
    //public float waitTimeR = 0.25f;
    //public bool Steping = false;

    //private AudioSource audioSrc;

    //private bool isMoving=false;



    //// Use this for initialization
    //void Start()
    //{

    //    audioSrc = GetComponent<AudioSource>();
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (isMoving && !Steping)
    //     {
    //        Invoke("FootStep", 0);
    //        Debug.Log("Steped");
    //     }

    //}

    //void FootStep()
    //{
    //    Steping = true;


    //    audioSrc.clip = FootStepSounds[Random.Range(0, FootStepSounds.Length)];
    //    audioSrc.PlayOneShot(audioSrc.clip);

    //    Invoke("ChangeStep", waitTime);


    //}

    //void ChangeStep()
    //{
    //    Steping = false;
    //}

    //public void SwitchIsMoving(bool state)
    //{
    //    isMoving = state;
    //}

    #endregion
}
