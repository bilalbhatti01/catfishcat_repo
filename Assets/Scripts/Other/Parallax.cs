﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    public float parallaxEffect;

    public GameObject cam;

    private float _length;
    private float _startPos;

    private void Start()
    {
        _startPos = transform.position.x;
        _length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    private void Update()
    {
        ParallaxEffect();
    }


    /* Custom Methods */
    private void ParallaxEffect()
    {
        float temp = (cam.transform.position.x * (1 - parallaxEffect));

        float dist = (cam.transform.position.x * parallaxEffect);

        transform.position = new Vector3(_startPos + dist, transform.position.y, transform.position.z);

        if( temp > _startPos + _length)
        {
            _startPos += _length;
        }

        else if(temp < _startPos - _length)
        {
            _startPos -= _length;
        }
    }
}
