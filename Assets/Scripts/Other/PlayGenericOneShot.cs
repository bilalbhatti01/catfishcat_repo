﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class PlayGenericOneShot : MonoBehaviour
{
    [SerializeField]
    [EventRef]
    private string soundEvent = null;

    public void PlayOneShotSoundEvent()
    {
        if(soundEvent != null)
        {
            RuntimeManager.PlayOneShot(soundEvent);
        }
    }
}
