﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointClickMovement : MonoBehaviour
{
    public static PointClickMovement instance;

    

    public Animator catAnim;

    public FootStepHandler footStepHandler;

    [SerializeField]
    private bool movementLock = false;

    [SerializeField]
    Camera mainCam;

    [SerializeField]
    float moveSpeed = 5f;

   

    private bool autoMove = false;

    Rigidbody2D rb;

    Vector3 touchPos, whereToMove;

    //[SerializeField]
    //bool isMoving = false;

    //float prevDistToTouchPos, currDisToTouchPos;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
       
        if(autoMove)
        {
            StartMovement();
        }

            //Movement();
        HoldMovement();

            
        

    }

    private void HoldMovement()
    {
        if(Input.GetMouseButton(0))
        {
            if (!movementLock)
            {
                StartMovement();
                
            }
        } 

        if(Input.GetMouseButtonUp(0))
        {
            StopCatMovement();
        }

    }

    private void StartMovement()
    {

        touchPos = mainCam.ScreenToWorldPoint(Input.mousePosition);
        touchPos.z = 0;


        if (touchPos.x - transform.position.x > 1.5f)
        {
            SunController.instance.MoveSunDown();

            //isMoving = true;

            catAnim.SetBool("Moving", true); /* Enables cat walk animation */

            footStepHandler.SwitchIsMoving(true); /* Enable footstep sounds */



       

       
            whereToMove = (touchPos - transform.position).normalized;
            rb.velocity = new Vector2(1 * moveSpeed, whereToMove.y * moveSpeed);
        }
       
    }

    

    private void StopCatMovement()
    {
        //isMoving = false;

        catAnim.SetBool("Moving", false); /* Disable Cat moving Animation */

        footStepHandler.SwitchIsMoving(false); /* Disable footstep sounds */

        rb.velocity = Vector2.zero;
    }

    public IEnumerator ChangeMovementState_Coroutine(bool state)
    {

        StopCatMovement();

        yield return null;

        movementLock = state;
        Debug.Log($" MovementLock Update = {state}");
    }

    public void SwitchMovementLockState(bool state)
    {
        //StartCoroutine(ChangeMovementState_Coroutine(state));

        movementLock = state;

        StopCatMovement();
    }

    public void TurnAutoMoveOn(bool state)
    {
        autoMove = state;
    }

    public bool GetLockStatus()
    {
        return movementLock;
    }

}
