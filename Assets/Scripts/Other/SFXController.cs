﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SFXController : MonoBehaviour
{
    public static SFXController instance;
    
    [SerializeField]
    [EventRef]
    private string uIMessengerStart_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIMessengerIncoming_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIMessengerSend_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIClick_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIPost_SoundEvent;

    [SerializeField]
    [EventRef]
    private string bubblePopUp_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIPhoto_SoundEvent;

    [SerializeField]
    [EventRef]
    private string uIInstagramIconStartVibration_SoundEvent;

    [SerializeField]
    [EventRef]
    private string doorSlam_SoundEvent;

    private void Awake()
    {
        instance = this;
    }

    public void PlaySoundEvent(string soundEvent)
    {
        if (soundEvent != null)
        {
            RuntimeManager.PlayOneShot(soundEvent);
        }
    }

    public void PlayUIClick()
    {
        PlaySoundEvent(uIClick_SoundEvent);
    }

    public void PlayUIPost()
    {
        PlaySoundEvent(uIPost_SoundEvent);
    }

    public void PlayUIConversationStart()
    {
        PlaySoundEvent(uIMessengerStart_SoundEvent);
    }

    public void PlayUIMessageIncoming()
    {
        PlaySoundEvent(uIMessengerIncoming_SoundEvent);
    }

    public void PlayUIMessegeSend()
    {
        PlaySoundEvent(uIMessengerSend_SoundEvent);
    }

    public void PlayBubblePop()
    {
        PlaySoundEvent(bubblePopUp_SoundEvent);
    }

    public void PlayUIPhoto()
    {
        PlaySoundEvent(uIPhoto_SoundEvent);
    }

    public void PlayinstaIconVibaration()
    {
        PlaySoundEvent(uIInstagramIconStartVibration_SoundEvent);
    }

    public void PlayDoorSlam()
    {
        PlaySoundEvent(doorSlam_SoundEvent);
    }




    #region commeneted out Old COde
    //public AudioClip uiMsgTone;

    //public AudioSource audioSrc;

    //public static SFXController instance;

    //private void Awake()
    //{
    //    instance = this;
    //}

    //public void PlayMsgTone()
    //{
    //    audioSrc.PlayOneShot(uiMsgTone);
    //}

    #endregion
}
