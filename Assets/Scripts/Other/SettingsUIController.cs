﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using DG.Tweening;

public class SettingsUIController : MonoBehaviour
{
    [Header(" Settings Panel : ")]
    public GameObject settingGO;
    public Button settingOpenBtn;

    public Button closeBtn;

    [Header(" Settings Tween Controls ")]
    public float OpenYPos;
    public float CloseYPos;
    public float tweenDuration;


    [Header(" Slider UI Elements ")]
    [SerializeField]
    private Slider masterSlider;

    [SerializeField]
    private Slider ambSlider;

    [SerializeField]
    private Slider sfxSlider;

    [SerializeField]
    private Slider musicSlider;

    [SerializeField]
    private string masterBusPath = "";

    [SerializeField]
    private string ambBusPath = "";

    [SerializeField]
    private string sfxBusPath = "";

    [SerializeField]
    private string musicBusPath = "";

    private FMOD.Studio.Bus masterBus;
    private FMOD.Studio.Bus ambBus;
    private FMOD.Studio.Bus sfxBus;
    private FMOD.Studio.Bus musicBus;

    private bool alreadyMovementLocked;

    private bool isOpen = false;

    public bool inMenu = false;



    public static SettingsUIController instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GetAllBuses();

        GetInitialVolume();

        AttachMethodsOnSliders();

        //settingBtn.onClick.AddListener(ToggleSettingPanel);

        MethodSubscribtion();

    }

    #region Sliders and FMOD

    private void GetAllBuses()
    {
        if (!masterBusPath.Equals(""))
        {
            masterBus = RuntimeManager.GetBus(masterBusPath);
        }

        if(!ambBusPath.Equals(""))
        {
            ambBus = RuntimeManager.GetBus(ambBusPath);
        }

        if (!sfxBusPath.Equals(""))
        {
            sfxBus = RuntimeManager.GetBus(sfxBusPath);
        }

        if (!musicBusPath.Equals(""))
        {
            musicBus = RuntimeManager.GetBus(musicBusPath);
        }


    }

    private void GetInitialVolume()
    {
        masterBus.getVolume(out float masterVolume);
        masterSlider.value = masterVolume * masterSlider.maxValue;

        ambBus.getVolume(out float ambVolume);
        ambSlider.value = ambVolume * ambSlider.maxValue;

        sfxBus.getVolume(out float sfxVolume);
        sfxSlider.value = sfxVolume * sfxSlider.maxValue;

        musicBus.getVolume(out float musicVolume);
        musicSlider.value = musicVolume * musicSlider.maxValue;
    }

    private void AttachMethodsOnSliders()
    {
        masterSlider.onValueChanged.AddListener(UpdateMasterVolume);

        ambSlider.onValueChanged.AddListener(UpdateAmbVolume);

        sfxSlider.onValueChanged.AddListener(UpdateSFXVolume);

        musicSlider.onValueChanged.AddListener(UPdateMusicVolume);
    }

    public void UpdateMasterVolume(float val)
    {
        masterBus.setVolume(val / masterSlider.maxValue);
    }

    public void UpdateAmbVolume(float val)
    {
        ambBus.setVolume(val / ambSlider.maxValue);
    }

    public void UpdateSFXVolume(float val)
    {
        sfxBus.setVolume(val / sfxSlider.maxValue);
    }

    public void UPdateMusicVolume(float val)
    {
        musicBus.setVolume(val / musicSlider.maxValue);
    }

    #endregion

    #region Settings Panel Related

    private void MethodSubscribtion()
    {
        if(!inMenu)
        {
            settingOpenBtn.onClick.AddListener(OpenSettingsPanel);

            closeBtn.onClick.AddListener(CloseSettingPanel);
        }
        else
        {
            settingOpenBtn.onClick.AddListener(InMenu_OpenPanel);

            closeBtn.onClick.AddListener(InMenu_ClosePanel);
        }
     
    }

    public void OpenSettingsPanel()
    {
        SFXController.instance.PlayUIClick();

        settingGO.GetComponent<RectTransform>().DOAnchorPosY(OpenYPos, tweenDuration).SetEase(Ease.OutBack);

        isOpen = true;

        if(PointClickMovement.instance.GetLockStatus() && isOpen!=true)
        {
            alreadyMovementLocked = true;
        }
        else
        {
            alreadyMovementLocked = false;
            PointClickMovement.instance.SwitchMovementLockState(true);
        }
    }

   

    public void CloseSettingPanel()
    {
        SFXController.instance.PlayUIClick();

        settingGO.GetComponent<RectTransform>().DOAnchorPosY(CloseYPos, tweenDuration).SetEase(Ease.InBack);
        
        isOpen = false;

        //if(!PostPicUIController.instance.GetPostStatus() && !MessengerUIController.instance.GetChatStatus())
        //{
        //    PointClickMovement.instance.SwitchMovementLockState(false);

        //}

        if(alreadyMovementLocked)
        {

        }
        else
        {
            PointClickMovement.instance.SwitchMovementLockState(false);
        }

    }

    public void InMenu_OpenPanel()
    {
        SFXController.instance.PlayUIClick();

        settingGO.GetComponent<RectTransform>().DOAnchorPosY(OpenYPos, tweenDuration).SetEase(Ease.OutBack);
    }

    public void InMenu_ClosePanel()
    {
        SFXController.instance.PlayUIClick();

        settingGO.GetComponent<RectTransform>().DOAnchorPosY(CloseYPos, tweenDuration).SetEase(Ease.InBack);
    }

    public void ToggleSettingPanel()
    {
        if(isOpen)
        {
            CloseSettingPanel();
        }
        else
        {
            OpenSettingsPanel();
        }
    }

    [ContextMenu(" Testing Open ")]
    public void TestOpen()
    {
        OpenSettingsPanel();
    }

    [ContextMenu(" Testing Close ")]
    public void TestClose()
    {
        CloseSettingPanel();
    }

    #endregion
}
