﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StoryTriggerCheck : MonoBehaviour
{
    public GameObject triggerObject;

    public TriggerEvent OnEnterTriggerZone_Event;
    private void OnTriggerExit2D(Collider2D collision)
    {
        

        Debug.Log("Entered Trigger Zone");
        OnEnterTriggerZone_Event.Invoke();

        if(triggerObject!=null)
        {
            triggerObject.SetActive(true);

        }
    }

}


[System.Serializable]
public class TriggerEvent : UnityEvent { };