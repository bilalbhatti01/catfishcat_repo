﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ThoughtBubbleUIController : MonoBehaviour
{
    public static ThoughtBubbleUIController instance;

    public GameObject thougthBubbleGO;

    public TMP_Text bubbleTxt;

    public float bubblePopTweenDuration;

    public Ease easeType;

    private void Awake()
    {
        instance = this;
    }

    private void OpenThoughtBubble()
    {

        SFXController.instance.PlayBubblePop();

        // thougthBubbleGO.transform.DOScale(new Vector3(8, 8, 1), bubblePopTweenDuration).SetEase(Ease.OutElastic);
        thougthBubbleGO.transform.DOScale(new Vector3(8, 8, 1), bubblePopTweenDuration).SetEase(easeType);
    }

    public void CloseThoughtBubble()
    {
        //SFXController.instance.PlayBubblePop();

        DOTween.Kill(thougthBubbleGO.transform);
        thougthBubbleGO.transform.DOScale(new Vector3(0, 0, 0), bubblePopTweenDuration).SetEase(Ease.InElastic);
    }

    public void HeartBeatBubbleTween()
    {
        thougthBubbleGO.transform.DOScale(new Vector3(9, 9, 1), 1f).SetLoops(-1,LoopType.Yoyo);
    }

    IEnumerator ThoughtBubbleOpeningTween()
    {
        OpenThoughtBubble();
        yield return new WaitForSeconds(1f);
        HeartBeatBubbleTween();
    }

    public void ChangeBubbleText(string msg)
    {
        bubbleTxt.text = msg;
    }

    public void PopOpenThoughtBubble()
    {
        StartCoroutine(ThoughtBubbleOpeningTween());
    }



    #region TEST CASE

    [ContextMenu(" Test Open Thought Bubble ")]
    public  void TestOpenTB()
    {
        //OpenThoughtBubble();
        StartCoroutine(ThoughtBubbleOpeningTween());
    }


    [ContextMenu(" Test Close Thought Bubble")]
    public void TestCloseTB()
    {
        CloseThoughtBubble();
    }

    #endregion

}
