﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class TriggerInteraction : MonoBehaviour
{

    public TriggerEvent OnTouchTrigger_Event; 

    private void OnMouseDown()
    {
        SFXController.instance.PlayUIPhoto();

        Debug.Log(" Pressed OnTouchTrigger_Event ");
        OnTouchTrigger_Event.Invoke();

        gameObject.SetActive(false);

        

    }

   
}
