﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TriggerTween : MonoBehaviour
{
    [Header(" Tween Controls ")]
    public float triggerTweenDuration;
    public Vector3 triggerTweenMaxScale;
    public Ease easeType;

    private void Start()
    {
        StartTriggerTween();
    }

    public void StartTriggerTween()
    {
        transform.DOScale(triggerTweenMaxScale, triggerTweenDuration).SetLoops(-1, LoopType.Yoyo).SetEase(easeType);
    }

    private void OnDestroy()
    {
        DOTween.Kill(gameObject.transform);
    }
}
