﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsManager : MonoBehaviour
{
    public static PrefsManager instance;

    private void Awake()
    {
        instance = this;
    }

    public void SaveDanger(float val)
    {
        PlayerPrefs.SetFloat("danger", val);
    }

    public float GetDanger()
    {
        return PlayerPrefs.GetFloat("danger");
    }

    public void ResetDanger()
    {
        PlayerPrefs.SetFloat("danger",0.0f);
    }
}
