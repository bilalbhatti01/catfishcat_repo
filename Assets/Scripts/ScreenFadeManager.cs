﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class ScreenFadeManager : MonoBehaviour
{
    public Image transitionImage;
    public TMP_Text daytext;
    

    public void FadeIn()
    {
        transitionImage.DOFade(0,1);
        daytext.DOFade(0, 1);
    }

    public void FadeOut()
    {
        transitionImage.DOFade(1, 1);
    }

    public void TextFadeout()
    {
        daytext.DOFade(1, 1);
    }

    public void BadEndFade()
    {

        //daytext.DOFade(1, 1);
        transitionImage.DOFade(1, 1);
    }

    [ContextMenu("Test Fade In")]
    public void TestFadeIn()
    {
        FadeIn();
    }

    [ContextMenu("Test Fade Out")]
    public void TestFadeOut()
    {
        FadeOut();
    }
}
