﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunController : MonoBehaviour
{
    public GameObject sunGo;

    public Color color1 ;

    public Camera cam;
    

    public float moveSpeed;

    public static SunController instance;

    private void Awake()
    {
        instance = this;
    }

    public void MoveSunDown()
    {
        sunGo.transform.position = new Vector3(sunGo.transform.position.x, sunGo.transform.position.y - (Time.deltaTime* moveSpeed) , 1);
        //cam.backgroundColor = Color.Lerp(cam.backgroundColor, color1, Time.deltaTime*moveSpeed);
    }
}
