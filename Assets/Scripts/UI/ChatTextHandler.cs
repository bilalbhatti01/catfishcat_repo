﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class ChatTextHandler : MonoBehaviour
{
    public TMP_Text chatTxt;

    public GameObject chatContainerGO;
    public GameObject avatarImgGO;

    
    public float scaleYMax;
    public float tweenDuration;
    public Ease easeTypeMessage;

    public Ease easeTypeAvatar;


    public void Start()
    {
        MessagePopTween();
    }

    public void DisplayMsg(string msg)
    {
        chatTxt.text = msg;

        Debug.Log("Message Updated");
    }

    private void MessagePopTween()
    {
        chatContainerGO.transform.DOScaleY(scaleYMax, tweenDuration).SetEase(easeTypeMessage);
        avatarImgGO.transform.DOScaleY(scaleYMax, tweenDuration).SetEase(easeTypeAvatar);
    }
}
