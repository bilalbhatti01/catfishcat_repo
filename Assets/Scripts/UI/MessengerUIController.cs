﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class MessengerUIController : MonoBehaviour
{
    [Header("UI Element References")]
    public GameObject chatContainerParent;
    public GameObject keyboardParent;
    public GameObject keyboardWholeGO;
    public GameObject chatWholeGO;


    [Header("Prefab References")]
    public GameObject catMsgPrefab;
    public GameObject fishMsgPrefab;
    public GameObject chatMsgOptionPrefab;

    [Header("Keyboard Tween Controls")]
    public float KbOpenYPos;
    public float KbCloseYPos;
    public float KbTweenDuration;

    [Header("Chat History Tween Controls")]
    public float cOpenYPos;
    public float cCloseYPos;
    public float cTweenDuration;

    public static MessengerUIController instance;

    private bool chatisOn;

    private void Awake()
    {
        instance = this;
    }

    #region Messages

    public void CreateMessage(bool myMsg, string msg , bool playUIsound = false)
    {
        

        /*--SFX SECTION--*/
        if(myMsg)
        {
             
            SFXController.instance.PlayUIMessegeSend();
        }
        else if(!myMsg)
        {
            SFXController.instance.PlayUIMessageIncoming();
        }

        /*~~SFX SECTION~~*/


        switch (myMsg)
        {
            case true:
                {
                    GameObject go = Instantiate(catMsgPrefab, chatContainerParent.transform);
                      go.GetComponent<ChatTextHandler>().chatTxt.text = msg;
                    break;
                }
            case false:
                {
                    GameObject go = Instantiate(fishMsgPrefab, chatContainerParent.transform);
                     go.GetComponent<ChatTextHandler>().chatTxt.text = msg;
                    break;
                }

        }
       

    }

    public void OpenMessenger()
    {
        chatisOn = true;

        ResponseBtnUIController.instance.FocusImage();

        chatWholeGO.GetComponent<RectTransform>().DOAnchorPosY(cOpenYPos, cTweenDuration);
    }

    public void CloseMessenger()
    {
        chatisOn = false;

        ResponseBtnUIController.instance.UnFocusImage();

        GameManager.instance.ChangeChatWaitState(false);
        chatWholeGO.GetComponent<RectTransform>().DOAnchorPosY(cCloseYPos, cTweenDuration);
    }

    public bool GetChatStatus()
    {
        return chatisOn;
    }

    public void DestroyMsgs()
    {
        foreach (Transform child in chatContainerParent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    #endregion

    #region Options

    public void CreateOption(string optionMsg, int number)
    {
        GameObject go = Instantiate(chatMsgOptionPrefab, keyboardParent.transform);

        go.GetComponentInChildren<TMP_Text>().text = optionMsg;

        go.GetComponent<Button>().onClick.AddListener(delegate { OptionBtn_OnClick(number); });
    }

    public void CreateMultipleButtons(string[] msgsArray, int[] valuesArray)
    {
        DestroyBtns();

        if (Random.Range(0, 2) == 0)
        {


            for (int i = 0; i < msgsArray.Length; i++)
            {
                CreateOption(msgsArray[i], valuesArray[i]);
            }
        }
        else
        {
            for (int i = msgsArray.Length - 1; i >= 0; i--)
            {
                CreateOption(msgsArray[i], valuesArray[i]);
            }
        }


    }

    public void OpenKeyboard()
    {
        keyboardWholeGO.GetComponent<RectTransform>().DOAnchorPosY(KbOpenYPos, KbTweenDuration);
    }

    public void CloseKeyboard()
    {
        keyboardWholeGO.GetComponent<RectTransform>().DOAnchorPosY(KbCloseYPos, KbTweenDuration);
    }

    public void OptionBtn_OnClick(int i)
    {
        Debug.Log($" {i} Was Clicked");

        ChatManager.instance.ChangeCatChatState(i);

    }

    public void DestroyBtns()
    {
        foreach (Transform child in keyboardParent.transform)
        {
            Destroy(child.gameObject);
        }
    }


    #endregion


    #region Test Cases

    [ContextMenu("Open Messenger")]
    public void TestOpenMessenger()
    {
        OpenMessenger();
    }

    [ContextMenu("Close Messenegr")]
    public void TestCloseMessenegr()
    {
        CloseMessenger();
    }


    [ContextMenu("Create Sample Cat Msg")]
    public void TestCreateCatMsg()
    {
        CreateMessage(true, "Hello I am Cat.");
    }

    [ContextMenu("Create Sample Fish Msg")]
    public void TestCreateFishMsg()
    {
        CreateMessage(false, "Hello I am Fish :3.");
    }

    [ContextMenu(" Open Keyboard ")]
    public void TestOpenKB()
    {
        OpenKeyboard();
    }

    [ContextMenu(" Close Keyboard ")]
    public void TestCloseKB()
    {
        CloseKeyboard();
    }

    [ContextMenu(" Create Sample Option Button. ")]
    public void TestCreateOptionBtn()
    {
        CreateOption("Sample Msg", 0);
    }

    #endregion

}
