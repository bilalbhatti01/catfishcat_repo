﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class PostPicUIController : MonoBehaviour
{
    public static PostPicUIController instance;

    [Header(" UI References ")]
    public GameObject postPanel;
    public GameObject postContainer;

    public Button locBtn;
    public TMP_Text locTxt;
    public Image postImg;
    public TMP_Text captionTxt;

    [Header(" Location Button Sprites")]
    public Sprite locOn_sprite;
    public Sprite locOff_sprite;

    [Header(" Sprites ")]
    public Sprite skylinePlaceholder;
    public Sprite icecreamPlayholder;
    public Sprite darkAlleyPlaceHolder;
    public Sprite parkBenchPlaceHolder;
    public Sprite lightBulbPlaceHolder;

    [Header("UI Tween Controls")]
    public float postContTweenSpeed = 0.5f;


    private bool locOn = true;

    private bool isPostOn;




    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        locBtn.onClick.AddListener(LocBtn_OnClick);
    }


    public void LocBtn_OnClick()
    {
        if (locOn)
        {
            locOn = false;
            locTxt.enabled = false;
            locBtn.gameObject.GetComponent<Image>().sprite = locOff_sprite;


        }
        else
        {
            locOn = true;
            locTxt.enabled = true;
            locBtn.gameObject.GetComponent<Image>().sprite = locOn_sprite;
        }
    }

    public bool GetLocationBool()
    {
        return locOn;
    }

    #region UI CONTROLS

    public void ChangeLocText(string txt)
    {
        locTxt.text = txt;
    }

    public void ChangeCaptionText(string txt)
    {
        captionTxt.text = txt;
    }

    public void ChangePostImage(Sprite img)
    {
        postImg.sprite = img;
    }

    public void ClosePost()
    {
        ResponseBtnUIController.instance.UnFocusImage();


        isPostOn = false;
        postContainer.GetComponent<RectTransform>().DOAnchorPosY(1600, 0.5f);
    }


    public void OpenPost()
    {
        ResponseBtnUIController.instance.FocusImage();

        isPostOn = true;
        postContainer.GetComponent<RectTransform>().DOAnchorPosY(100, 0.5f);
    }

    public bool GetPostStatus()
    {
        return isPostOn;
    }

    #endregion

    #region Post 

    public void Post1()
    {
        ChangePostImage(skylinePlaceholder);
        ChangeLocText(" at 5th Avenue ");
        ChangeCaptionText(" Having a rough day... I hate it here! #runaway#straykitten#sunset ");
    }

    public void Post2()
    {
        ChangePostImage(icecreamPlayholder);
        ChangeLocText(" at baker street ice cream ");
        ChangeCaptionText(" An icecream a day keeps the depression away! #moodbooster#yummy#nom ");
    }

    public void Post3()
    {
        ChangePostImage(darkAlleyPlaceHolder);
        ChangeLocText(" at Meow district ");
        ChangeCaptionText(" This alley looks so scary, I wonder who is hiding in there... #bravecat#riskybusiness");
    }

    public void Post4()
    {
        ChangePostImage(parkBenchPlaceHolder);
        ChangeLocText(" at 6th Avenue Park ");
        ChangeCaptionText(" Escaping all the trouble from home in nature. #parklife#naturecat#escape ");
    }

    public void Post5()
    {
        ChangePostImage(lightBulbPlaceHolder);
        ChangeLocText(" at 5th Avenue ");
        ChangeCaptionText(" Thank you for being the light which guides me through this dark time. #hopefulcat#companion ");
    }



    #endregion

    #region Test Cases:


    [ContextMenu("Test Open Post")]
    public void TestOpenPost()
    {
        

        OpenPost();

    }
    [ContextMenu("Test Close Post")]
    public void TestClosePost()
    {
        ClosePost();
    }

    [ContextMenu("Test Post 1")]
    public void TestPost1()
    {

        Post1();
        OpenPost();
    }

    [ContextMenu("Test Post 2")]
    public void TestPost2()
    {
        Post2();
        OpenPost();
    }

    [ContextMenu("Test Post 3")]
    public void TestPost3()
    {
        Post3();
        OpenPost();
    }


    [ContextMenu("Test Post 4")]
    public void TestPost4()
    {
        Post4();
        OpenPost();
    }


    [ContextMenu("Test Post 5")]
    public void TestPost5()
    {
        Post5();
        OpenPost();
    }

    #endregion 


}
