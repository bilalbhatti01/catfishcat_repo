﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ResponseBtnUIController : MonoBehaviour
{
    public static ResponseBtnUIController instance;

    public GameObject yesNoContainer;
    public GameObject answerContainer;
    public GameObject instaContainer;

    public Button yesBtn;
    public Button noBtn;
    public Button answerBtn;

    public Image blackImage;

    public Button instaBtn;

    public Ease easeType;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        yesBtn.onClick.AddListener(OnPostBtnPressed);

        answerBtn.onClick.AddListener(OnAnswerBtnPressed);

        instaBtn.onClick.AddListener(OnInstaButtonPressed);

    }

    [ContextMenu("Close YesNo")]
    public void CloseYesNoContainer()
    {
        yesNoContainer.GetComponent<RectTransform>().DOAnchorPosY(-208, 0.5f);
    }

    [ContextMenu("Open YesNo")]
    public void OpenYesNoContainer()
    {
        yesNoContainer.GetComponent<RectTransform>().DOAnchorPosY(0, 0.5f);
    }

    [ContextMenu("Close Answer")]
    public void CloseAnswerContainer()
    {
        answerContainer.GetComponent<RectTransform>().DOAnchorPosY(-208, 0.5f);
    }

    [ContextMenu("Open Answer")]
    public void OpenAnswerContainer()
    {
        answerContainer.GetComponent<RectTransform>().DOAnchorPosY(0, 0.5f);
    }

    [ContextMenu("Open Insta")]
    public void OpenInstaIconContainer()
    {
        FocusImage();

#if UNITY_WEBGL || UNITY_STANDALONE 
        SFXController.instance.PlayinstaIconVibaration();
#elif UNITY_ANDROID || UNITY_IPHONE
        SFXController.instance.PlayUIConversationStart();
        Handheld.Vibrate();
        Handheld.Vibrate();
#endif

        instaContainer.GetComponent<RectTransform>().DOAnchorPosY(640, 1f).SetEase(easeType);
        VibrateInstaIcon();
    }

    [ContextMenu("Close Insta")]
    public void CloseInstaIconContainer()
    {
        UnFocusImage();

        DOTween.Kill(instaBtn.transform);

        instaContainer.GetComponent<RectTransform>().DOAnchorPosY(-208, 1f).SetEase(easeType);
    }

    private void VibrateInstaIcon()
    {
        instaBtn.GetComponent<RectTransform>().DOPunchRotation(new Vector3(0,0,15),5).SetLoops(-1,LoopType.Incremental);
    }

    public void FocusImage()
    {
        blackImage.DOFade(0.5f,0.5f);
        blackImage.raycastTarget = true;
    }

    public void UnFocusImage()
    {
        blackImage.DOFade(0,0.5f);
        blackImage.raycastTarget = false;
    }

    private void OnPostBtnPressed()
    {
        SFXController.instance.PlayUIPost();

        ChatManager.instance.SwitchLocationState(GameManager.instance.Chapter - 1 , PostPicUIController.instance.GetLocationBool() );

        GameManager.instance.ChangePostWaitState(false);
    }

    private void OnAnswerBtnPressed()
    {
        SFXController.instance.PlayUIClick();

        ChatManager.instance.ChangeCatAnswerState();

    }

    private void OnInstaButtonPressed()
    {
        SFXController.instance.PlayUIClick();

        CloseInstaIconContainer();

        ChatManager.instance.ChangeCatAnswerState();
    }

}
