﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WayPointManager : MonoBehaviour
{
    public static WayPointManager instance;

    public List<WayPointObject> wayPoints;

    private void Awake()
    {
        instance = this;
    }

    public int FindRandomUnTaken()
    {
        var UnchoosenWayPoints = wayPoints.Where(i => i.taken==false);

        var selection = UnchoosenWayPoints.ElementAtOrDefault(new System.Random().Next() % UnchoosenWayPoints.Count());

        int index = wayPoints.FindIndex(a => a.Equals(selection));

        wayPoints[index].taken = true;

        return index;
    }

    public void ReleaseWaypoint(int index)
    {
        wayPoints[index].taken = false;
    }

}

[System.Serializable]
public class WayPointObject
{
    public GameObject wayPoint;

    public bool taken;
}
